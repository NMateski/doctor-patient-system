# Doctor-patient system:
## Office:
### Features:
* The office can register one or multiple patients
    * Input: A patient
    * Output: The registered patient with an Id
* The office can register one or multiple doctors
    * Input: A doctor
    * Output: The registered doctor with an id

### General Rules:
* There can be multiple offices, each with a type
* There can be only one office per type
* A patient can be registered in multiple offices
* A doctor can be registered to only 1 office
* Date format dd-mm-yyyy

### Model Rules:
* The office has an id, a name, description and a type.
* All fields are required.
* The id is a long.
* Name (max characters 200, only letters are allowed and empty spaces)
* Description (max 5000 chars, all characters are allowed)
* Type (can be Dentist, General Practitioner, Psychiatrist, Emergency, etc. you can add your own types :) )
* The office can't register the same patient twice (same name, date of birth and social security number)
* The office can't register the same doctor twice (same name, date of birth and specialization)
	

## Doctor:
### Features:
 * A doctor can view a list of meetings for a given date.
    * Input: date
    * Output: a list of meetings grouped by time slots. 
        * Each meeting should have: patient id, patient full name, patient date of birth, start time of the meeting
 * A doctor can accept the meeting or decline it
    * Input: meeting id
    * Output: NONE
 * A doctor should be able to access his patients recrods at an office.
    * Input: patient id, date from, date to, patient full name (case insensitive), patient date of birth, a list of record ids. 
        * Patient id is optional
        * Date to is optional. 
        * Patient id is optional.
        * Patient full name is optional
        * Patient date of birth is optional
        * List of record ids is optional
    * Output: a list of records
 * The doctor should be able to upload a CSV (comma separated file) as a list of new patient records. 
    * Input: 
        * The file contains one or more patient records. Each row in the file should be a new patient record
        * The patient record  has the following fields/columns:
            patientId, doctorId, timestampOfStartOfTheMeeting, doctorRecommendation, recepiesList in format: [{drugname, quantity/packs, timesPerDayToBeTaken}]
            ex. 21,1,2021-01-22T12:22:34,2021-01-22T12:42:34,"Some doctor reccomendation here",[{"some drug name", 2, 3}, {"drug name 44", 23, 1}];
            2,12,2021-01-10T15:02:44,2021-01-10T15:31:44,"Some other doctor reccomendation here",[{"some other drug name", 2, 3}, {"drug name 2", 1, 3}];
    * Output: The number of created records

### General Rules:
* A doctor's meeting time slot is always 30 minutes.
* A doctor has a maximum of 16 time slots per day from Monday to Friday
* A doctor has a maximum of 8 time slots on Saturday and Sunday
* A doctor can have meetings with multiple patients in one day, but only 1 patient in 1 time slot
* Date format dd-mm-yyyy

### Model rules:
* A doctor has an id, a name, specialization, how many years working, date of birth
* All the fields are required
* The id is a long.
* Name (200 chars max, only letters and empty spaces are allowed)
* Specialization (100 chars, only letters and empty spaces are allowed)
* How many years working (this is calculated from the start date of the doctor at the office)


## Patient:
### Features:
* A patient can request a meeting with a doctor
    * Input: patient id, doctor id, date of meeting, meeting start time (meeting slot)
    * Output: message indicating that a succesfull or unsuccessful request
* A patient should be able to acces his patient records.
    * Input: patientId. Optionally a date can also be entered in the format dd-mm-yyyy, to get only a list of records for that date.
    * Output: A list of records.
* A patient should be able to check the details from a meeiting with the doctor (the recommendation and the recepy list if any).
    * Input: patientId and meeting id
    * Output: Details of the meeting: doctor's recommendation, doctor id, doctor full name, a list of recepies (if any), date and time of meeting
 	 	
### General rules:
* A patient can request only free time slots with a doctor

### Model rules:
* A patient has an id, a name, date of birth, social security number
* The id is a long.
* Name (200 chars max, only letters and empty spaces are allowed)
* Social security number (max 30 chars, can have letters, numbers and dashes "-" )
 	 

## Record:
### Model rules: 
* Every patient/doctor meeting is saved as a record. This record has the patient id, doctor id, date and time of the meeting, how long it lasted, what was the doctor's reccomendation, a list of recepies.


### Recepy/drug model rules:
* Every recepy/drug has a name, quantity, times taken in a day
* All fields are required
* Name (200 chars max, only letters and empty spaces are allowed)
* Quantity (min 1, max 500)
* Times taken per day (min 1, max 5)



# Some application rules:
* You **_SHOULD_** have separate classes that manage/implement the features and rules for: the office, doctor, patient and records. E.g. PatientService, DoctorService, OfficeService, RecordService, etc.
* You **_CAN_** have a separate class called Repository, whhich will hold all the logic and data for the offices, doctors, patients, records, etc. This will basically handle all the retrieval and saving/persistence of data for a specific model (doctor, patient, office, etc.). You **_DON'T_** have to do it this way, but you can try if you like
* If there are issues (or if some of the rules/features/models of the application don't seem ok, please report it on Skype and we can update the specification)  